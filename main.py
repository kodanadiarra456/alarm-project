from fastapi import FastAPI
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles
from alarm.routes import router as alaram_router

app = FastAPI()

app.include_router(alaram_router)

app.mount("/AdminLTE-3.2.0/dist", StaticFiles(directory="AdminLTE-3.2.0/dist"), name="dist")
app.mount("/AdminLTE-3.2.0/build", StaticFiles(directory="AdminLTE-3.2.0/build"), name="build")
app.mount("/AdminLTE-3.2.0/docs", StaticFiles(directory="AdminLTE-3.2.0/docs"), name="docs")
app.mount("/AdminLTE-3.2.0/pages", StaticFiles(directory="AdminLTE-3.2.0/pages"), name="pages")
app.mount("/AdminLTE-3.2.0/plugins", StaticFiles(directory="AdminLTE-3.2.0/plugins"), name="plugins")

@app.get('/')
def health_check():
    return JSONResponse(content={"status": "Running!"})