import pandas as pd


def separator(data):
    # with open(file_name) as file:
    #     data = file.read()
        

    ### We found out that every line start with a type of ID, *A....
    ### So we used it as our first separator to get our lines(records)

    separator = data.split("*A")



    ### INFO.CPLT: Info complementaire, we supposed to keep all data after this key word to futher action
    ###
    Line_separed = []
    word_to_match = "INFO.CPLT:\\r\\n"


    ### we used / as our second separator to get our column
    ### there's two cases, the first when we found info.cplt, the spits stop when we found INFO.CPLT
    ### The second case, just do everything !!!

    for sep in separator:
        
        if word_to_match in sep:
            sindex = sep.find(word_to_match)
            sep = "A"+ sep
            newsep = sep[:sindex].split("/")
            newsep.append(sep[sindex:])
            Line_separed.append(newsep)
        else:    
            sep = "A"+sep
            Line_separed.append(sep.split("/"))
        

    ### the first four column doesn't have a title in the code so assign them one
    ### We regroup the INFO COMP in complementaire

    id = []
    nbr = []
    date = []
    hour = []
    complementaire = []


    lines = []

    for x in Line_separed[1:]:
        id.append(x[0])
        nbr.append(x[1])
        date.append(x[2])
        hour.append(x[3])
        
        line = []
        for element in x[4:]:
            if "=" in element and "INFO.CPLT:" not in element:
                key = element.split('=')[0].strip()
                value = element.split('=')[1].strip()
                
                line.append((key,value))
                
                
            elif "=" not in element and "INFO.CPLT:" not in element:
                key = element.strip()
                value = None
                line.append((key,value))
            
        ## Separate data with = 
        #line = dict((element.split('=')[0].strip(), element.split('=')[1].strip()) if "=" in element and "INFO.CPLT:" not in element else (element.strip(), None)
        #          for element in x[4:])
        
        if "INFO.CPLT:" in x[-1]:
            complementaire.append(x[-1])
        else:
            complementaire.append("")
        line = dict(line)
        lines.append(line)

    result1 = {
        "ID" : id,
        "number" : nbr,
        "Date" : date,
        "Heure" : hour,
        "Info_Complementaire":complementaire
    }

    df = pd.DataFrame(result1)

    for x in range(len(df)):
        for y in list(lines[x].keys()):
            if y not in df.columns:
                df[y] = ""
                df.iloc[x, df.columns.get_loc(y)] = lines[x][y]
            else:
                df.iloc[x, df.columns.get_loc(y)] = lines[x][y]
                
                
    df["Info_complementaire2"] = df["Info_Complementaire"]
    df = df.drop("Info_Complementaire", axis=1)

    ### Clean Unwanted Character
    def removenp(word):
        word = word.replace("\\r","")
        word = word.replace("\\n","")
        word = word.replace("!","")

        return word

    columns_to_clean = ["EVENT", "AGEO","TEXAL","Info_complementaire2"]

    for column in columns_to_clean:
        df[column] = df[column].apply(removenp)

    ### UT HS presence 

    #print(lines[4])

    for x in range(len(df)):
        try:
            if lines[x]["UT HS"] == None:
                df.iloc[x,df.columns.get_loc("UT HS")] = 1
            
        except KeyError as error:
            df.iloc[x,df.columns.get_loc("UT HS")] = 0
            pass
        
    # df.to_csv("alarms.csv")
    return df