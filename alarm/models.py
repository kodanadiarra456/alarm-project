from typing import List

from fastapi import FastAPI, APIRouter
from pydantic import BaseModel, Field
import databases
import sqlalchemy
from datetime import datetime

DATABASE_URL = "sqlite:///./alarm.db"

metadata = sqlalchemy.MetaData()

database = databases.Database(DATABASE_URL)

alarm = sqlalchemy.Table(
    "alarm",
    metadata,
    # ID,number,Date,Heure,N,TYP,CAT,EVENT,NCEN,AM,AGEO,TEXAL,OBJET,AFCN,AFUR,UT HS,Info_complementaire2
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("id_alarm", sqlalchemy.String),
    sqlalchemy.Column("number", sqlalchemy.Integer),
    sqlalchemy.Column("Date", sqlalchemy.String),
    sqlalchemy.Column("Heure", sqlalchemy.String),
    sqlalchemy.Column("N", sqlalchemy.Integer),
    sqlalchemy.Column("TYP", sqlalchemy.String),
    sqlalchemy.Column("CAT", sqlalchemy.String),
    sqlalchemy.Column("EVENT", sqlalchemy.String),
    sqlalchemy.Column("NCEN", sqlalchemy.String),
    sqlalchemy.Column("AM", sqlalchemy.String),
    sqlalchemy.Column("AGEO", sqlalchemy.String),
    sqlalchemy.Column("TEXAL", sqlalchemy.String),
    sqlalchemy.Column("OBJET", sqlalchemy.String),
    sqlalchemy.Column("AFCN", sqlalchemy.String),
    sqlalchemy.Column("AFUR", sqlalchemy.String),
    sqlalchemy.Column("UT_HS", sqlalchemy.String),
    sqlalchemy.Column("Info_complementaire2", sqlalchemy.String),
)

engine = sqlalchemy.create_engine(
    DATABASE_URL
)

metadata.create_all(engine)