from fastapi import APIRouter, Depends, UploadFile, Request, File, HTTPException
from fastapi.responses import HTMLResponse, RedirectResponse, JSONResponse
from fastapi.templating import Jinja2Templates
from tortoise.contrib.fastapi import HTTPNotFoundError
from typing import List
from pydantic import BaseModel
import pydantic
import sqlalchemy
import os
from collections import Counter
from alarm.models import database, alarm
from Clean_data.separator import separator

router = APIRouter(
    prefix="/alarm",
    tags=["Alarm"],
    responses={404: {"description": "Not found"}}
)

message = ""
templates = Jinja2Templates(directory="AdminLTE-3.2.0")

class AlarmIn(BaseModel):
    file: UploadFile

class Alarm(BaseModel):
    id: int
    id_alarm: str
    number: int
    Date: str
    Heure: str
    N: int
    TYP: str
    CAT: str
    EVENT: str
    NCEN: str
    AM: str
    AGEO: str
    TEXAL: str
    OBJET: str
    AFCN: str
    AFUR: str
    UT_HS: str
    Info_complementaire2: str

@router.on_event("startup")
async def connect():
    await database.connect()

@router.on_event("shutdown")
async def shutdown():
    await database.disconnect()

@router.post('/import')
async def store(request: Request, file: UploadFile = File(...)):
  try:
    # Étape 1: Récupération du fichier
    content = await file.read()
    
    # Étape 2: Copie du fichier dans le dossier "temp_files"
    temp_dir = "temp_files"
    os.makedirs(temp_dir, exist_ok=True)  # Créer le dossier s'il n'existe pas
    
    temp_file_path = os.path.join(temp_dir, file.filename)
    
    with open(temp_file_path, "wb") as temp_file:
        temp_file.write(content)
    
    # Étape 3: Lecture du fichier à partir du dossier temporaire
    with open(temp_file_path, "r") as read_file:
        # Étape 4: Récupération du contenu du fichier
        file_content = read_file.read()
            
    # Étape 5: Suppression du fichier temporaire
    os.remove(temp_file_path)

    datas_to_store = separator(data=file_content)
    # print(datas_to_store)
    for label, row in datas_to_store.iterrows():
      query = alarm.insert().values(
          id_alarm=row["ID"],
          number=row["number"],
          Date=row["Date"],
          Heure=row["Heure"],
          N=row["N"],
          TYP=row["TYP"],
          CAT=row["CAT"],
          EVENT=row["EVENT"],
          NCEN=row["NCEN"],
          AM=row["AM"],
          AGEO=row["AGEO"],
          TEXAL=row["TEXAL"],
          OBJET=row["OBJET"],
          AFCN=row["AFCN"],
          AFUR=row["AFUR"],
          UT_HS=row["UT HS"],
          Info_complementaire2=row["Info_complementaire2"],
      )

      await database.execute(query)

    # return templates.TemplateResponse(
    #     "index.html", 
    #     {
    #         "request": request,
    #         "success": True,
    #         "message": "Les données ont été enregistrés avec succès."
    #     }
    # )
    message = "Les données ont bien étés enregistrés."  
    
    return RedirectResponse(
      url="/alarm",
      status_code=303
    )
  except Exception as e:
    raise HTTPException(status_code=500, detail=str(e))

@router.get('', response_class=HTMLResponse, response_model=List[Alarm])
async def get_all(request: Request):
    query = alarm.select()
    all_get = await database.fetch_all(query)

    # Utilisez Counter pour compter les occurrences de chaque élément dans la colonne spécifiée
    column = "TEXAL"
    count_elements = Counter(entry[column] for entry in all_get)

    return templates.TemplateResponse(
        "index.html", 
        {
            "request": request,
            "alarms": all_get,
            # "message": "messagesssssss",
            "texal_counter": count_elements
        }
    )

@router.get("/delete", responses={404 : {'model': HTTPNotFoundError}})
async def delete_alarms(request: Request):
  # query = alarm.select()
  # all_get = await database.fetch_all(query)
  delete_query = sqlalchemy.delete(alarm)
  await database.execute(delete_query)

  # return templates.TemplateResponse(
  #         "index.html", 
  #         {
  #             "request": request,
  #             "success": True,
  #             "message": "Les données ont été supprimées avec succès."
  #         }
  #     )
  return RedirectResponse(
      url="/alarm"
  )

# @router.get('')
# async def name(request: Request):
#     return templates.TemplateResponse("index.html", {"request": request, "name": "Kodana"})


# class AlarmIn(BaseModel):
#     name: str = Field(...)

# class Alarm(BaseModel):
#     id: int
#     name: str
#     date_created: datetime 

# class Message(BaseModel):
#     message : str


# @router.post('/import/')
# async def create():
#     query = register.insert().values(
#         # name=r.name,
#         date_created=datetime.utcnow()
#     )
#     record_id = await database.execute(query)
#     query = register.select().where(register.c.id == record_id)
#     row = await database.fetch_one(query)
#     return {**row}

# @app.get('/register/{id}', response_model=Register)
# async def get_one(id: int):
#     query = register.select().where(register.c.id == id)
#     user = await database.fetch_one(query)
#     return {**user}

# @app.get('/register/', response_model=List[Register])
# async def get_all():
#     query = register.select()
#     all_get = await database.fetch_all(query)
#     return all_get

# @app.put('/register/{id}', response_model=Register)
# async def update(id: int, r: RegisterIn = Depends()):
#     query = register.update().where(register.c.id == id).values(
#         name=r.name,
#         date_created=datetime.utcnow(),
#     )
#     row = await database.fetch_one(query)
#     query = register.select().where(register.c.id == id)
#     row = await database.fetch_one(query)
#     return {**row}
